1. Pada class “com.hascode.tutorial.entity”, Line Covarage yang tercapai yaitu 67% atau sebanyak 6 baris kode yang dijalankan saat tes dilakukan, dari kesulurahan 9 baris kode. Sedangkan untuk Mutation coverage, tingkat mutasi coverage yang tercapai adalah 67% dari 3, ini berarti bahwa dari 3 perubahan yang dibuat pada kode selama proses tes mutasi, hanya 2 perubahan yang terdeteksi oleh tes yang ada.

Pada class “com.hascode.tutorial.service, Line coverage yang tercapai yaitu 100% yang artinya seluruh baris code dijalankan saat tes dilakukan. Sedangkan untuk Mutation coverage, tingkat mutasi coverage yang tercapai adalah 75% dari 4, ini berarti bahwa dari 4 perubahan yang dibuat pada kode selama proses tes mutasi, hanya 3 perubahan yang terdeteksi oleh tes yang ada.


2. Kesimpulan dari gambar soal nomor 2 yaitu Line Coverage tercapai seluruhnya, akan tetapi mutation coverage-nya tidak tercover seluruhnya yaitu pada line 15,16.
